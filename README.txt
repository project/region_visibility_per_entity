Region Visibility Per Node provides extra settings for every node to define the
visibility of theme regions whenever that nodes page is viewed.

All regions are visible by default and can be hidden by deselecting the
respective checkbox.
